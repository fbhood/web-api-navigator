(function() {
  const body = document.querySelector("body");

  const mockup = `
  <footer id="connection-status-footer" class="fixed-bottom">
    <div id="status_wrapper" >
      <em>Connection Status:</em> <span id="status"></span>
    </div>
    <div id="stats">
      <ul ></ul>
    </div>
  </footer>
  
  <style>
 
  #connection-status-footer {
    font-size: 18px;
    width: 100vw;
    background: #ededed;
    margin: 0;
    padding: 1rem
  }
  a {
    display: block;
  }
  .center {
    text-align: center;
  }
  .lead {
    font-size: 1.25rem;
  }
  .online {
    background: green;
    padding: 0.25rem;
    border-radius: 10px;
    color: white;
    font-size: 0.8rem;
  }
  .offline {
    background: red;
    padding: 0.25rem;
    border-radius: 10px;
    color: white;
    font-size: 0.8rem;
  }
  .fixed-bottom {
    position: fixed;
    bottom: 0;
    margin: 2rem;
  }
  #stats ul {
    font-size: 12px;
    z-index: 1;
    padding: 0.8rem;
    list-style: none;
    left: 0;
  }
  #status_wrapper {
    right: 0;
    font-size: 12px;

  }
  
  
  </style>
  `;
  body.insertAdjacentHTML("beforeend", mockup);

  const status = document.querySelector("#status_wrapper");
  const stats = document.querySelector("#stats > ul");
  const articleImg = document.querySelector("article img");

  // Check initial connection status

  status.innerHTML = `<span class="${
    navigator.onLine === true ? "online" : "offline"
  }">${navigator.onLine === true ? "Connected" : "Disconnected"}</span>`;

  // Listen to internet connection changes
  window.addEventListener("online", function() {
    if (navigator.onLine === true) {
      onMarkup = `<span class="online">Online</span>`;
      status.innerHTML = onMarkup;

      // save current status in the local storage
      localStorage.setItem(
        "current-connection-status",
        `online @ ${new Date()}`
      );
    }
  });
  window.addEventListener("offline", function() {
    if (navigator.onLine == false) {
      offMarkup = `<span class="offline">Offline</span>`;
      status.innerHTML = offMarkup;

      // save previous status in the local storage
      localStorage.setItem(
        "previous-connection-status",
        `online @ ${new Date()}`
      );

      localStorage.setItem(
        "current-connection-status",
        `offline @ ${new Date()}`
      );
    }
  });

  // show connection stats
  let prevStatus = localStorage.getItem("previous-connection-status");
  let currStatus = localStorage.getItem("current-connection-status");
  stats.insertAdjacentHTML(
    "beforeend",
    `<li>Type: ${navigator.connection.effectiveType}</li>`
  );
  stats.insertAdjacentHTML(
    "beforeend",
    `<li>Download Speed: ${navigator.connection.downlink}</li>`
  );

  /* LOCAL STORAGE API - show data stored in the localStorage
   *****************************************/
  stats.insertAdjacentHTML(
    "beforeend",
    `<li> Prev Status Stored: ${prevStatus}</li>`
  );
  stats.insertAdjacentHTML(
    "beforeend",
    `<li> Current Status Stored: ${currStatus}</li>`
  );
  stats.insertAdjacentHTML("beforeend", `<li> ${navigator.userAgent}</li>`);

  /* serve different image if the connection if 2g, 3g, or 4g
   *************************************************************/

  function checkType() {
    if ("4g" === navigator.connection.effectiveType) {
      console.log("user on a 4g connection");
      let img = (articleImg.src = "./img/beautiful_girl_museum_4g.JPG");
    }
    if ("3g" === navigator.connection.effectiveType) {
      console.log("user on a 3g connection");
      let img = (articleImg.src = "./img/beautiful_girl_museum_3g.JPG");
    }
    if ("2g" === navigator.connection.effectiveType) {
      console.log("user on a 2g connection");
      let img = (articleImg.src = "./img/beautiful_girl_museum_2g.JPG");
    }
  }
  checkType();
})();
